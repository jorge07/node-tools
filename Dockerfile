FROM node:5

RUN npm i -g bower grunt-cli gulp-cli

# Define working directory.
WORKDIR /app

# Define default command.
CMD ["bash"]